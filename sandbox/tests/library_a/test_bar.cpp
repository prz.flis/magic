#include <gtest/gtest.h>

#include "library_a/bar.h"

TEST(test_bar, check)
{
    ASSERT_EQ(2, 2);
}

TEST(test_bar, get_2)
{
    ASSERT_EQ(2, library_a::bar::get_2());
}