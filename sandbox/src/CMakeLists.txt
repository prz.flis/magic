cmake_minimum_required(VERSION 3.5)

add_executable(sandbox main.cpp)

add_subdirectory(library_a)

target_link_libraries (sandbox library_a)