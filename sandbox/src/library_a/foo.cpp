#include "foo.h"

#include <iostream>


namespace library_a{
namespace foo{

void hello()
{
    std::cout << "Hello world\n";
}

}
}
