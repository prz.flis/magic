#include "library_a/foo.h"
#include "library_a/bar.h"



int main()
{
    library_a::foo::hello();
    library_a::bar::world();
    return 0;
}